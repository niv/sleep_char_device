/*
 * my_first.char.h
 *
 * Copyright (C) Niv B.
 *
 */

#ifndef _SLEEPY_H_
#define _SLEEPY_H_

#include <linux/wait.h>
#include <linux/sched.h>

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>	
#include <linux/slab.h>	
#include <linux/fs.h>		
#include <linux/errno.h>	
#include <linux/types.h>	
#include <linux/proc_fs.h>
#include <linux/fcntl.h>	
#include <linux/seq_file.h>
#include <linux/cdev.h>

#include <asm/system.h>	
#include <asm/uaccess.h>
 
#include <linux/mutex.h> 
#include <linux/ioctl.h>

#define NAME_DEVICE "sleepy" // name of the device we'll be using

#define SIZE_KERNEL_BUFFER  512 // internal buffer

#define PRINTKN printk(KERN_NOTICE "char device unregistered")

/* file API */
int 		char_open(struct inode *inode, struct file *sfile);
int 		char_release(struct inode *inode, struct file *sfile);
ssize_t 	char_read(struct file *sfile, char __user *buf, size_t count, loff_t *f_pos);
ssize_t 	char_write(struct file *sfile, const char __user *buf, size_t count, loff_t *f_pos);
loff_t  	char_llseek(struct file *sfile, loff_t off, int whence);

/* private functions */
static void 	setup_cdev(void);

/* driver entry/exit methods */
int 		init_driver(void);
void 		exit_driver(void);

#endif /* _SLEEPY_H_ */