#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>

#define DEVICE_PATH "/dev/sleepy"
#define COUNT 20

/******************************************************************************/
int main()
{
	int fd;
	char write_buffer[COUNT];
	char read_buffer[COUNT];
	int count = 0;
	int i;
	
	memset (read_buffer,0,COUNT);
	for(i=0;i<COUNT;i++){
		write_buffer[i]='a'+i;
	}
	
	fd=open(DEVICE_PATH, O_RDWR);
	assert(fd>=0);

	// write to char device
	count = write(fd,write_buffer,COUNT);
	printf("%d bytes written.\n", count);

	
	close(fd);
	return 0;

}
/******************************************************************************/