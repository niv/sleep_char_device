#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>

#define DEVICE_PATH "/dev/sleepy"
#define COUNT 20

/******************************************************************************/
int main()
{
	int fd;
	char write_buffer[COUNT];
	char read_buffer[COUNT];
	int count = 0;
	int i;
	
	// init buffers
	memset (read_buffer,0,COUNT);
	for(i=0;i<COUNT;i++) {
		write_buffer[i]='a'+i;
	}
		
	// open device file
	fd=open(DEVICE_PATH, O_RDWR);
	assert(fd>=0);

	// read from char device
	count = read(fd,read_buffer,COUNT);
	printf(" read done %s\n", count==COUNT ? "correctly" : "incorectly");

	
	close(fd);
	return 0;

}
/******************************************************************************/